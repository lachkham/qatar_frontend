import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { EquipeService } from '../services/Equipe/equipe.service';

import { FileInput } from 'ngx-material-file-input';
import { ImageService } from '../services/image/image.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppSettings } from '../settings/app.settings';
import { Staff } from '../model/Personnes/Staff';
import { StaffService } from '../services/personnes/staff/staff.service';
interface DialogData {
  data: Staff;
}
@Component({
  selector: 'app-add-staff',
  templateUrl: './add-staff.component.html',
  styleUrls: ['./add-staff.component.css']
})
export class AddStaffComponent implements OnInit {
  joueurForm = this.fb.group({
    firstName: [null, Validators.required],
    imageFile: [null],
    lastName: [null, Validators.required],
    datenaiss: [null, Validators.required],
    equipe: [null, Validators.required],
    cin: [null, Validators.compose([
      Validators.required, Validators.minLength(7), Validators.maxLength(8)])
    ],
    role: ['', Validators.required]
  });
  loaded = false;
  widthValue = 0;
  hasUnitNumber = false;
  photo: FileInput;
  displayedPhoto: any = '../assets/inconnu.jpg';
  equipes = [];
  staff = new Staff();

  constructor(private fb: FormBuilder,
              private equipeService: EquipeService,
              private staffService: StaffService,
              private imageService: ImageService,
              public dialogRef: MatDialogRef<AddStaffComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {}


  ngOnInit() {
      if (this.data.data != null) {
        console.log(this.data.data)

      this.setForm();
      }

      
      this.equipeService.findAll().subscribe(data => {
        this.equipes = data;
        
      });
  }

  setForm() {
    this.joueurForm.get('cin').setValue(this.data.data.cin);
    this.joueurForm.get('cin').disable();
    this.joueurForm.get('role').setValue(this.data.data.fonction);
    this.joueurForm.get('equipe').setValue(this.data.data.equipe.idEquipe);
    this.joueurForm.get('datenaiss').setValue(new Date(this.data.data.dateNaiss));
    this.joueurForm.get('lastName').setValue(this.data.data.prenom);
    this.joueurForm.get('firstName').setValue(this.data.data.nom);
    this.displayedPhoto = AppSettings.APP_URL + 'Images/Low/' + this.data.data.image.id ;
  }

  onSubmit() {
    const dn = new Date(this.joueurForm.get('datenaiss').value);

    this.staff.nom = this.joueurForm.get('firstName').value;
    this.staff.prenom = this.joueurForm.get('lastName').value;
    this.staff.fonction = this.joueurForm.get('role').value;
    this.staff.cin = this.joueurForm.get('cin').value;
    this.staff.equipe =  this.equipes.filter(x => x.idEquipe === this.joueurForm.get('equipe').value)[0];
    this.staff.dateNaiss = dn.getTime() ;
    this.photo = this.joueurForm.get('imageFile').value;
    
    console.log(this.staff);

      if (this.joueurForm.valid) {
        
        if (this.data.data == null || this.data==undefined) {

        this.imageService.AddOne(this.photo.files[0]).subscribe(data => {
          this.staff.image = data ;
        },
        err => {
          console.log(err);
        },
        () => {
          this.staffService.AddOne(this.staff).subscribe(data => {
            console.log(data);
          });
        });
        this.dialogRef.close();

       
      
    } else {
      

        if (this.photo != null) {
          console.log("photo null");
          this.imageService.AddOne(this.photo.files[0]).subscribe(data => {
            this.staff.image = data ;
            console.log(data);
          },
          err => {
            console.log(err);
          },
          () => {
            this.staffService.UpdateOne(this.staff).subscribe(data => {
              console.log(data);
            });
          });
          this.dialogRef.close();
        } else {
          console.log("photo not null");

          this.staff.image = this.data.data.image;
          this.staffService.UpdateOne(this.staff).subscribe(data => {
            console.log(data);
          });
          this.dialogRef.close();

        }
      }

    }

  }
  

  onChange() {
    this.photo = this.joueurForm.get('imageFile').value;
    console.log(this.joueurForm.get('imageFile').value);
    const  myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.displayedPhoto =  myReader.result;
    };
    myReader.readAsDataURL(this.photo.files[0]);

  }
  load() {
    this.loaded = true;
    this.widthValue = 150;
  }


}
