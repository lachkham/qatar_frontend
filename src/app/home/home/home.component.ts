import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';




import { AddPartieComponent } from 'src/app/add-partie/add-partie.component';
import { AddSpectateurComponent } from 'src/app/add-spectateur/add-spectateur.component';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }
  openDialog(): void {

 
 

    const dialogRef = this.dialog.open(AddSpectateurComponent, {

      width: '500px',
      data: {data : null}
    });

    
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

    });
  }

}
