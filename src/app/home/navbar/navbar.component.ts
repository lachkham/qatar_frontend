import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { LoginService } from 'src/app/services/personnes/login.service';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AddPersonnesComponent } from 'src/app/add-personnes/add-personnes.component';
import { AppSettings } from 'src/app/settings/app.settings';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  constructor(public dialog: MatDialog,private loginService: LoginService,private router: Router) { }
  isNavbarCollapsed=true;
   loggedIn = false; 
   nom="";
   prenom="";
   image="";
  ngOnInit(): void {
    $(window).on("scroll",function(){
      if($(window).scrollTop()){
        $('nav').addClass('bg-dark fade-in');
      }else{
        $('nav').removeClass('bg-dark fade-in');
      }
    });
    this.loggedIn=this.loginService.checkSession()
    this.nom=localStorage.getItem('nom');
    this.prenom=localStorage.getItem('prenom');
    this.image=AppSettings.APP_URL+"Images/High/"+localStorage.getItem('image');


  }
logout(){
  this.loginService.logout();
}

openAddDialog(): void {
  const dialogRef = this.dialog.open(AddPersonnesComponent, {
    width: '50wv',
    data:null
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log(result);
  if(result){
    this.router.navigate(["/login/"])      }
  });
}

}
