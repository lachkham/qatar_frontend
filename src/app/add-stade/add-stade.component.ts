
import { Component, OnInit, Inject } from '@angular/core';
import { FileInput } from 'ngx-material-file-input';
import { Validators, FormBuilder } from '@angular/forms';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { AppSettings } from 'src/app/settings/app.settings';
import { Stade } from 'src/app/model/Stade';
import { StadeService } from 'src/app/services/stade/stade.service';
interface DialogData {
  data: Stade;
}
@Component({
  selector: 'app-add-stade',
  templateUrl: './add-stade.component.html',
  styleUrls: ['./add-stade.component.css']
})
export class AddStadeComponent implements OnInit {

  stadeForm = this.fb.group({
    
    nomStade: [null, Validators.required],
    capacite: [null, Validators.required],
    
    
    
  });
  loaded = false;
  widthValue = 0;
  hasUnitNumber = false;
 
  
  
stade=new Stade();


  constructor(private fb: FormBuilder,
   private stadeService :StadeService,
    public dialogRef: MatDialogRef<AddStadeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}


    ngOnInit() {
      if (this.data != null) {
      this.setForm();
      }

     
  }
  setForm() {
    this.stadeForm.get('nomStade').setValue(this.data.data.nomStade);
    
    this.stadeForm.get('capacite').setValue(this.data.data.capacite);
    
    



  }







  onSubmit() {
    
    this.stade.idStade = 0;
    this.stade.nomStade = this.stadeForm.get('nomStade').value;
    this.stade.capacite = this.stadeForm.get('capacite').value;
    console.log(this.stade);


    if (this.stadeForm.valid) {
      
      
        this.stadeService.AddOne(this.stade).subscribe(data => {
          console.log(data);
        },
        err =>{},
        ()=>{
              this.dialogRef.close();
        });
     

    }

  

}

  



}
