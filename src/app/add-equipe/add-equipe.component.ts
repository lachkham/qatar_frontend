import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { EquipeService } from '../services/Equipe/equipe.service';
import { Equipe } from '../model/Equipe';
import { FileInput } from 'ngx-material-file-input';
import { ImageService } from '../services/image/image.service';
import { AddJoueurComponent } from '../add-joueur/add-joueur.component';
import { MatDialogRef } from '@angular/material';


@Component({
  selector: 'app-add-equipe',
  templateUrl: './add-equipe.component.html',
  styleUrls: ['./add-equipe.component.css']
})
export class AddEquipeComponent {
  equipeForm = this.fb.group({
    equipe: [null, Validators.required],
    imageFile: [null, Validators.required],

  });

  photo: FileInput;
  displayedPhoto: any = '../assets/Flag.png';
  equipes = [];
  hasUnitNumber = false;
  equipe = new Equipe();
  constructor(     private fb: FormBuilder,
                   private equipeService: EquipeService,
                   private imageService: ImageService,
                   public dialogRef: MatDialogRef<AddJoueurComponent>) {}

  onSubmit() {
    this.equipe.idEquipe = 0;
    this.equipe.pays = this.equipeForm.get('equipe').value;


    if (this.equipeForm.valid) {
      this.photo = this.equipeForm.get('imageFile').value;
      this.imageService.AddOne(this.photo.files[0]).subscribe(data => {
        this.equipe.drapeau = data ;
      },
      err => {
        console.log(err);
      },
      () => {
        this.equipeService.AddOne(this.equipe).subscribe(data => {
          console.log(data);
        },
        err =>{},
        ()=>{
              this.dialogRef.close();
        });
      });

    }

  }

  onChange() {
    this.photo = this.equipeForm.get('imageFile').value;
    console.log(this.equipeForm.get('imageFile').value);
    const  myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.displayedPhoto =  myReader.result;
    };
    myReader.readAsDataURL(this.photo.files[0]);

  }
}
