import { Equipe } from './Equipe';
import { Stade } from './Stade';

export class Partie {
    idPartie: number;
    eq1: Equipe;
    eq2: Equipe;
    stade: Stade;
    scoreEq1: number;
    scoreEq2: number;
    date: number;
    tours: string;
}
