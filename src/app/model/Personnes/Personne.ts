import { Image } from '../Image';

export class Personne {
    cin: number;
    nom: string;
    prenom: string;
    username: string;
    password: string;
    dateNaiss: number;
    image: Image;
    role: string[];

}
