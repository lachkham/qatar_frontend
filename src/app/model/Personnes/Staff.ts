import { Personne } from './Personne';
import { Equipe } from '../Equipe';

export class Staff extends Personne {
    equipe: Equipe;
    fonction: string;
}
