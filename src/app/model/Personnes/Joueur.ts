import { Personne } from './Personne';
import { Equipe } from '../Equipe';

export class Joueur extends Personne {
    equipe: Equipe;
    poste: string;
    blessure: boolean;
}
