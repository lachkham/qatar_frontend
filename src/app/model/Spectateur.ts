import { Personne } from './Personnes/Personne';
import { Partie } from './Partie';

export class Spectateur {
    codeTicket: number;
    spectateur: Personne;
    partie: Partie;

 
}
