import { Component, OnInit, ViewChild } from '@angular/core';
import { SpectateurService } from '../services/spectateur/spectateur.service';
import { MatPaginator, MatSort, MatTable, MatTableDataSource, MatDialog } from '@angular/material';
import { Spectateur } from '../model/Spectateur';
import { TicketComponent } from '../ticket/ticket.component';

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.css']
})
export class TicketListComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Spectateur>; 

  constructor(private spectateursService: SpectateurService,public dialog: MatDialog) { }

  displayedColumns = ['numTicket','Nom','Prenom','Partie','qr'];
  dataSource = new MatTableDataSource([]);



  spectateurs;
  ngOnInit(): void {
    this.spectateursService.findAll().subscribe(tickets => {

      this.dataSource= new MatTableDataSource(tickets);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort= this.sort;

      this.table.dataSource=this.dataSource;
    })



  }

  openDialog(row : Spectateur): void {
    const dialogRef = this.dialog.open(TicketComponent, {
      width: '700px',
      data: {data: row}
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}
