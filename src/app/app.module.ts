import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddJoueurComponent } from './add-joueur/add-joueur.component';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatRadioModule } from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { ReactiveFormsModule } from '@angular/forms';
import { AddEquipeComponent } from './add-equipe/add-equipe.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { EquipeListeComponent } from './equipe-liste/equipe-liste.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { JoueurListeComponent } from './joueur-liste/joueur-liste.component';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import {MatIconModule} from '@angular/material/icon';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatRippleModule} from '@angular/material/core';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDialogModule} from '@angular/material/dialog';
import { EquipeDialogComponent } from './equipe-liste/equipe-dialog/equipe-dialog.component';
import {MatTabsModule} from '@angular/material/tabs';
import { NavbarComponent } from './home/navbar/navbar.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import { HomeComponent } from './home/home/home.component';
import {MatGridListModule} from '@angular/material/grid-list';
import { CarouselComponent } from './home/carousel/carousel.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FooterComponent } from './home/footer/footer.component';
import {MatStepperModule} from '@angular/material/stepper';

import { AddPersonnesComponent } from './add-personnes/add-personnes.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';

import { AddStaffComponent } from './add-staff/add-staff.component';
import { StaffListComponent } from './staff-list/staff-list.component';
import { AddStadeComponent } from './add-stade/add-stade.component';
import { AddPartieComponent } from './add-partie/add-partie.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { PartieListeComponent } from './partie-liste/partie-liste.component';
import { AddSpectateurComponent } from './add-spectateur/add-spectateur.component';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { MatMenuModule } from '@angular/material/menu';
import { LayoutModule } from '@angular/cdk/layout';
import { NavigationComponent } from './dashboard/navigation/navigation.component';
import { MatListModule } from '@angular/material/list';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './guards/auth-guard.service';
import { LoginGuardService } from './guards/login-guard.service';
import { QRCodeModule } from 'angular2-qrcode';
import { TicketListComponent } from './ticket-list/ticket-list.component';
import { TicketComponent } from './ticket/ticket.component';
import { PrintLayoutComponent } from './print-layout/print-layout.component';

@NgModule({
  declarations: [
    AppComponent,
    AddJoueurComponent,
    AddEquipeComponent,
    EquipeListeComponent,
    JoueurListeComponent,
    EquipeDialogComponent,
    NavbarComponent,
    HomeComponent,
    CarouselComponent,
    FooterComponent,
    AddPersonnesComponent,
    AddStaffComponent,
    StaffListComponent,
    AddStadeComponent,
    AddPartieComponent,
    PartieListeComponent,
    AddSpectateurComponent,
    DashboardComponent,
    NavigationComponent,
    LoginComponent,
    TicketListComponent,
    TicketComponent,
    PrintLayoutComponent
  ],
  imports: [
    BrowserModule,
    QRCodeModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule,
    MatStepperModule,
    MatRadioModule,
    MatCardModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MaterialFileInputModule,
    MatIconModule,
    MatExpansionModule,
    MatRippleModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatTabsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatGridListModule,
    NgbModule,
    MatSelectModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule, MatMenuModule, LayoutModule, MatListModule,
    MatSnackBarModule

  ],
  providers: [MatDatepickerModule,AuthGuardService,LoginGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
