import { Component, OnInit, Inject } from '@angular/core';
import { Partie } from 'src/app/model/Partie';
import { Validators, FormBuilder } from '@angular/forms';
import { EquipeService } from 'src/app/services/Equipe/equipe.service';
import { PartieService } from 'src/app/services/partie/partie.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { StadeService } from 'src/app/services/stade/stade.service';
import { Equipe } from '../model/Equipe';






interface DialogData {
  data: Partie;
}

@Component({
  selector: 'app-add-partie',
  templateUrl: './add-partie.component.html',
  styleUrls: ['./add-partie.component.css']
})
export class AddPartieComponent implements OnInit {

  partieForm = this.fb.group({
    equipe1: [null, Validators.required],
    equipe2: [null,Validators.required],
    stade: [null, Validators.required],
    scoreEq1: [0],
    scoreEq2: [0],
    date: [null, Validators.required ],
    tours: [null, Validators.required]
  });


  loaded = false;
  widthValue = 0;
  hasUnitNumber = false;
  
  equipes1:Equipe[] = [];
  equipes2 : Equipe[]=[];
  stades=[];
  partie = new Partie();

  constructor(private fb: FormBuilder,
              private equipeService: EquipeService,
              private partieService:PartieService,
              private stadeService:StadeService,
              public dialogRef: MatDialogRef<AddPartieComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {}


  ngOnInit() {
      if (this.data!=null) {
        console.log(this.data.data)

      this.setForm();
      }

      this.equipeService.findAll().subscribe(data => {
        this.equipes1 = data;
        
      });
      this.equipeService.findAll().subscribe(data => {
        this.equipes2 = data;
      });

    this.stadeService.findAll().subscribe(data=> {
      this.stades=data;
    })



  }

  setForm() {
    this.partieForm.get('equipe1').setValue(this.data.data.eq1.idEquipe);
    
    this.partieForm.get('equipe2').setValue(this.data.data.eq2.idEquipe);
    this.partieForm.get('stade').setValue(this.data.data.stade.idStade);
    this.partieForm.get('date').setValue(new Date(this.data.data.date));
    this.partieForm.get('scoreEq1').setValue(this.data.data.scoreEq1);
    this.partieForm.get('scoreEq2').setValue(this.data.data.scoreEq2);
    this.partieForm.get('tours').setValue(this.data.data.tours)
    
  }
  

  onSubmit() {
    this.partie.idPartie=0;
    const dn = new Date(this.partieForm.get('date').value);

    this.partie.eq1 = this.equipes1.filter(x => x.idEquipe === this.partieForm.get('equipe1').value)[0];
    this.partie.eq2 = this.equipes2.filter(x => x.idEquipe === this.partieForm.get('equipe2').value)[0];
    this.partie.stade = this.stades.filter(x => x.idStade === this.partieForm.get('stade').value)[0];
    this.partie.scoreEq1 = parseInt(this.partieForm.get('scoreEq1').value);
    this.partie.scoreEq2 = parseInt(this.partieForm.get('scoreEq2').value);
    this.partie.date = dn.getTime() ;
    this.partie.tours=this.partieForm.get('tours').value;
   
    console.log(this.partie);
   







    if (this.partieForm.valid) {
      if(this.data!=null && this.data.data!=null){
        this.partie.idPartie=this.data.data.idPartie;
        this.partieService.UpdateOne(this.partie).subscribe(data => {
          console.log(data);
        },
        err =>{},
        ()=>{
              this.dialogRef.close();
        });
      }else{
        this.partieService.AddOne(this.partie).subscribe(data => {
          console.log(data);
        },
        err =>{},
        ()=>{
              this.dialogRef.close();
        });
      }
      
      

    }


  }

onChangeEquipe1(){


  
}
onChangeEquipe2(){

}



  load() {
    this.loaded = true;
    this.widthValue = 150;
  }




}
