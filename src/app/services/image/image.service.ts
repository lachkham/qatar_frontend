import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Image } from 'src/app/model/Image';
import { AppSettings } from 'src/app/settings/app.settings';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  controller = 'Images/';

  constructor(private http: HttpClient) { }

  getOne(id): Observable<Image> {
    return this.http.get<Image>(AppSettings.APP_URL + this.controller  + id);
  }

  AddOne(selectedFile: File): Observable<Image> {

    console.log(selectedFile);

    // FormData API provides methods and properties to allow us easily prepare form data to be sent with POST HTTP requests.
    const uploadImageData = new FormData();
    uploadImageData.append('imageFile', selectedFile, selectedFile.name);
    return this.http.post<Image>(AppSettings.APP_URL + this.controller , uploadImageData);

  }
}
