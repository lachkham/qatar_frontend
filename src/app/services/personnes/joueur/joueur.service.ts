import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppSettings } from 'src/app/settings/app.settings';
import { Joueur } from 'src/app/model/Personnes/Joueur';

@Injectable({
  providedIn: 'root'
})
export class JoueurService {
  constructor(private http: HttpClient) {}

  controller = 'Joueur/';
  
  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization':"Bearer " +localStorage.getItem('xAuthToken') });
   options = { headers: this.headers };

  findAll(): Observable<any> {
    return this.http.get(AppSettings.APP_URL + this.controller);
  }

  findAllByIdEquipe(idEquipe: Number): Observable<any> {
    return this.http.get(AppSettings.APP_URL + this.controller +'equipe/'+idEquipe);
  }


  getOne(id): Observable<Joueur> {
    return this.http.get<Joueur>(AppSettings.APP_URL + this.controller + id);
  }

  AddOne(body: Joueur): Observable<Joueur> {
    return this.http.post<Joueur>(AppSettings.APP_URL + this.controller, body,this.options);
  }

  DeleteOne(id) {
    return this.http.delete(AppSettings.APP_URL + this.controller + id,this.options);
  }
  UpdateOne(body: Joueur) {
    return this.http.put<Joueur>(AppSettings.APP_URL + this.controller, body,this.options);
  }
}
