import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppSettings } from 'src/app/settings/app.settings';
import { Personne } from 'src/app/model/Personnes/Personne';

@Injectable({
  providedIn: 'root'
})
export class PersonneService {

  constructor(private http: HttpClient) {}

  controller = 'Personne/';

  findAll(): Observable<any> {
    return this.http.get(AppSettings.APP_URL + this.controller);
  }

  getOne(id): Observable<Personne> {
    return this.http.get<Personne>(AppSettings.APP_URL + this.controller + id);
  }
  getCurrentUser(): Observable<Personne> {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization':"Bearer " +localStorage.getItem('xAuthToken') });
  let options = { headers: headers };
    return this.http.get<Personne>(AppSettings.APP_URL + this.controller+"user/",options);
  }

  AddOne(body: Personne): Observable<Personne> {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization':"Bearer " +localStorage.getItem('xAuthToken') });
  let options = { headers: headers };
  
    return this.http.post<Personne>(AppSettings.APP_URL + this.controller+"Inscription", body);
    
  }
  AddOneAdmin(body: Personne): Observable<Personne> {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization':"Bearer " +localStorage.getItem('xAuthToken') });
  let options = { headers: headers };
  
    return this.http.post<Personne>(AppSettings.APP_URL + this.controller+"addAdmin/", body,options);
    
  }
  UpdateOne(body: Personne): Observable<Personne> {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization':"Bearer " +localStorage.getItem('xAuthToken') });
  let options = { headers: headers };
  
    return this.http.put<Personne>(AppSettings.APP_URL + this.controller, body,options);
  }
  DeleteOne(id) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization':"Bearer " +localStorage.getItem('xAuthToken') });
  let options = { headers: headers };
  
    return this.http.delete(AppSettings.APP_URL + this.controller + id,options);
  }

}
