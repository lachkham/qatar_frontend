import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { AppSettings } from 'src/app/settings/app.settings';
import { Observable } from 'rxjs';
import { Personne } from 'src/app/model/Personnes/Personne';
import { Token } from 'src/app/model/Personnes/Token';

export class loginRequest {
  username: string;
  password: string;
}


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }
  controller = 'login';

  sendCredential(username : string, password : string) : Observable <Token>{
    let url = AppSettings.Login_URL+this.controller; 
    
/*		let encodedCredentials = btoa(username+ ":" + password);
		let basicHeader = "Basic " + encodedCredentials; 
		let headers = new HttpHeaders({
			"Content-Type" : 'application/x-www-form-urlencoded',
			"Authorization" : basicHeader
		});
		console.log(headers);
*/
      let body = new loginRequest();
      body.password=password;
      body.username=username;

   return this.http.post<Token>(url, body);
  }

  checkSession(){
  
	if(localStorage.getItem('xAuthToken')!=null || localStorage.getItem('xAuthToken') !=undefined){
    return true
  }

	return false;
}

logout(){
  localStorage.clear();
  location.reload();

}



}
