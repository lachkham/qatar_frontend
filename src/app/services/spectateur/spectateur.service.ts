import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppSettings } from 'src/app/settings/app.settings';
import { Spectateur } from 'src/app/model/Spectateur';

@Injectable({
  providedIn: 'root'
})
export class SpectateurService {

  constructor(private http: HttpClient) {}

  controller = 'Spectateurs/';
  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization':"Bearer " +localStorage.getItem('xAuthToken') });
   options = { headers: this.headers };

  findAll(): Observable<any> {
    return this.http.get(AppSettings.APP_URL + this.controller,this.options);
  }

  getOne(id): Observable<Spectateur> {
    return this.http.get<Spectateur>(AppSettings.APP_URL + this.controller + id,this.options);
  }

  AddOne(body: Spectateur): Observable<Spectateur> {
    return this.http.post<Spectateur>(AppSettings.APP_URL + this.controller, body,this.options);
  }

  DeleteOne(id) {
    return this.http.delete(AppSettings.APP_URL + this.controller + id,this.options);
  }

}
