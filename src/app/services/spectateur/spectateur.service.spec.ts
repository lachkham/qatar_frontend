import { TestBed } from '@angular/core/testing';

import { SpectateurService } from './spectateur.service';

describe('SpectateurService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SpectateurService = TestBed.get(SpectateurService);
    expect(service).toBeTruthy();
  });
});
