import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppSettings } from 'src/app/settings/app.settings';
import { Equipe } from 'src/app/model/Equipe';

@Injectable({
  providedIn: 'root'
})
export class EquipeService {

  constructor(private http: HttpClient) {}

  controller = 'Equipe/';
   headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization':"Bearer " +localStorage.getItem('xAuthToken') });
   options = { headers: this.headers };

  findAll(): Observable<any> {
    return this.http.get(AppSettings.APP_URL + this.controller);
  }

  getOne(id): Observable<Equipe> {
    return this.http.get<Equipe>(AppSettings.APP_URL + this.controller + id);
  }

  AddOne(body: Equipe): Observable<Equipe> {
    return this.http.post<Equipe>(AppSettings.APP_URL + this.controller, body,this.options);
  }

  DeleteOne(id) {
    return this.http.delete(AppSettings.APP_URL + this.controller + id,this.options);
  }
}
