import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppSettings } from 'src/app/settings/app.settings';
import { Stade } from 'src/app/model/Stade';
@Injectable({
  providedIn: 'root'
})
export class StadeService {

  constructor(private http: HttpClient) {}

  controller = 'Stade/';

  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization':"Bearer " +localStorage.getItem('xAuthToken') });
   options = { headers: this.headers };

  findAll(): Observable<any> {
    return this.http.get(AppSettings.APP_URL + this.controller);
  }

  getOne(id): Observable<Stade> {
    return this.http.get<Stade>(AppSettings.APP_URL + this.controller + id);
  }

  AddOne(body: Stade): Observable<Stade> {
    return this.http.post<Stade>(AppSettings.APP_URL + this.controller, body,this.options);
  }

  DeleteOne(id) {
    return this.http.delete(AppSettings.APP_URL + this.controller + id,this.options);
  }
}
