import { Component, OnInit } from '@angular/core';
import { EquipeService } from './services/Equipe/equipe.service';
import { JoueurService } from './services/personnes/joueur/joueur.service';
import { ImageService } from './services/image/image.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private equipeService: EquipeService, private joueurService: JoueurService, private imageService: ImageService) {}
  retrievedImage: any ;
ngOnInit() {

}
}
