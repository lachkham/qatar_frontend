import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { LoginService } from 'src/app/services/personnes/login.service';
import { AppSettings } from 'src/app/settings/app.settings';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {
  opned=true;

  nom="";
   prenom="";
   image="";

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver,private loginService: LoginService) {}

ngOnInit(){
  this.nom=localStorage.getItem('nom');
  this.prenom=localStorage.getItem('prenom');
  this.image=AppSettings.APP_URL+"Images/High/"+localStorage.getItem('image');
  ///////////////////////////////////////    
  window.innerWidth;
  if (window.innerWidth<=720){
      this.opned=false;
  }else{
      this.opned=true;
  }
}
onResize(event?) {
  window.innerWidth;
  if (window.innerWidth<=720){
      this.opned=false;
  }else{
      this.opned=true;
  }

}

  logout(){
    this.loginService.logout();
  }
}
