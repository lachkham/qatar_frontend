import { AfterViewInit, Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { StaffListDataSource, StaffListItem } from './staff-list-datasource';
import { MatDialog } from '@angular/material';
import { StaffService } from '../services/personnes/staff/staff.service';
import { Staff } from '../model/Personnes/Staff';
import { AppSettings } from '../settings/app.settings';
import { AddStaffComponent } from '../add-staff/add-staff.component';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-staff-list',
  templateUrl: './staff-list.component.html',
  styleUrls: ['./staff-list.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class StaffListComponent implements  OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<StaffListItem>;
  dataSource: StaffListDataSource;
  @Input() idEquipe:Number;
  role=localStorage.getItem("role");

  expandedElement: Staff | null;
  age;
  loaded = false;
  widthValue = 0;
  photo: any;

  constructor(private staffService: StaffService,
    public dialog: MatDialog){}
 

    tabLoaded="none";      
    initTabLoaded(){
      this.tabLoaded="none"
    }   

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['Nom', 'Prenom', 'fonction', 'equipe','add'];

  ngOnInit() {
    this.tabLoaded="none";         

  if(this.idEquipe===undefined || this.idEquipe===null){
    this.staffService.findAll().subscribe(staffs => {
     this.dataSource = new StaffListDataSource(staffs);
     console.log(this.tabLoaded);
    },
    err => {
      console.log(err);
    },
    () => {
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = this.dataSource;
      this.tabLoaded="block";         

    });
  }else {
    this.staffService.findAllByIdEquipe(this.idEquipe).subscribe(staffs => {
      this.dataSource = new StaffListDataSource(staffs);
 
      console.log(this.tabLoaded);
     },
     err => {
       console.log(err);
     },
     () => {
       this.dataSource.sort = this.sort;
       this.dataSource.paginator = this.paginator;
       this.table.dataSource = this.dataSource;
       this.tabLoaded="block";         
 
     });
  }
  }

  click(row: Staff) {


    if (this.expandedElement === row) {
      this.expandedElement = null;
  } else {
      this.loaded = false;
      this.widthValue = 0;
      this.photo = AppSettings.APP_URL + 'Images/Low/' + row.image.id ;
      this.expandedElement = row;
      const timeDiff = Math.abs(Date.now() - row.dateNaiss);
      this.age = Math.floor((timeDiff / (1000 * 3600 * 24)) / 365.25);

  }

}

load() {
  this.loaded = true;
  this.widthValue = 65;
}

openDialog(row): void {
  const dialogRef = this.dialog.open(AddStaffComponent, {
    width: '500px',
    data: {data: row}
  });

}


  
}
