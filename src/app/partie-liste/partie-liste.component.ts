import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { PartieListeDataSource, PartieListeItem } from './partie-liste-datasource';
import { PartieService } from '../services/partie/partie.service';
import { Partie } from '../model/Partie';
import { AddPartieComponent } from '../add-partie/add-partie.component';
import { MatDialog } from '@angular/material';
import { AddSpectateurComponent } from '../add-spectateur/add-spectateur.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-partie-liste',
  templateUrl: './partie-liste.component.html',
  styleUrls: ['./partie-liste.component.css']
})
export class PartieListeComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<PartieListeItem>;
  dataSource: PartieListeDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['eq1','Status','eq2',"add"];
  displayedPhoto="https://qatar-web.herokuapp.com/v1/Images/High/";


constructor( private partieService:PartieService,public dialog: MatDialog, private router:Router){}


role=localStorage.getItem("role");
action;
  ngOnInit() {
if(this.role=='ROLE_ADMIN'){
this.action="Modifier Partie";
}else{
  this.action='Acheter ticket'
}


    this.partieService.findAll().subscribe(data => {
        this.dataSource = new PartieListeDataSource(data);
        console.log(data);
    },
    err => {
      console.log(err);
    },
    () => {
         console.log(this.dataSource);
         this.dataSource.sort = this.sort;
         this.dataSource.paginator = this.paginator;
         this.table.dataSource = this.dataSource;
    }
       );
    
  }

  ngAfterViewInit() {
    
  }

  convertDate(row: Partie){
   let date = new Date(row.date);

    return date.toLocaleDateString()+', '+date.toLocaleTimeString();
  }
  openAddDialog(): void {
    const dialogRef = this.dialog.open(AddPartieComponent, {
      width: '500px',
      data: null
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
    });
  }

  openDialog(row): void {

if (this.role=='ROLE_CLIENT'){
    const dialogRef = this.dialog.open(AddSpectateurComponent, {
      width: '700px',
      data: {data:null,partie: row}
    });
  }else{
if(this.role=='ROLE_ADMIN'){
  const dialogRef = this.dialog.open(AddPartieComponent, {
    width: '700px',
    data: {data: row}
  });
}else{
  this.router.navigate(['/connexion'])
}
}

  }
}
