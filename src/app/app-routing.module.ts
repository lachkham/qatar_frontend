import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home/home.component';
import { LoginComponent } from './login/login.component';
import { NavigationComponent } from './dashboard/navigation/navigation.component';
import { LoginGuardService } from './guards/login-guard.service';
import { AuthGuardService } from './guards/auth-guard.service';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'connexion', component: LoginComponent,canActivate:[LoginGuardService] },
  { path: 'dashboard', component:NavigationComponent, canActivate:[AuthGuardService]}


 ];

@NgModule({
  imports: [RouterModule.forRoot(routes, 
    { 
    useHash: true,
    onSameUrlNavigation: 'reload'
  } 
)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
