import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/personnes/login.service';
import { MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import * as jwt_decode from "jwt-decode";
import { PersonneService } from '../services/personnes/personne/personne.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private credential = {'username':'', 'password':''}; 
	private loggedIn = false; 

  addressForm = this.fb.group({
    username: [null, Validators.required],
    password: [null, Validators.required],
  });

  constructor(private fb: FormBuilder,private loginService: LoginService,private personneService:PersonneService,private _snackBar: MatSnackBar) { }

  ngOnInit() {
   this.loggedIn=this.loginService.checkSession()
  }

  onSubmit(){
  

  }


  loginUser(event){
    this.credential.username =this.addressForm.get("username").value;
    this.credential.password =this.addressForm.get("password").value;
    if(this.credential.username!=null && this.credential.password!=null && this.credential.username!='' && this.credential.password!=''){
    console.log(this.credential);
    this.loginService.sendCredential(this.credential.username, this.credential.password).subscribe(
  		res => {
        console.log(res);


                localStorage.setItem("xAuthToken", res.token);
                console.log(localStorage);
                this.loggedIn = true;

              
               
              this.personneService.getCurrentUser().subscribe(res=>{

                localStorage.setItem("nom", res.nom);
                localStorage.setItem("prenom", res.prenom);
                localStorage.setItem("image", res.image.id+"");
                localStorage.setItem("role", res.role[0]);
                localStorage.setItem("datenaiss", res.dateNaiss+"");
                localStorage.setItem("username", res.username);
                console.log(localStorage.getItem('role'))
                console.log(localStorage.getItem('username'))
                
                location.reload();

              });

                
        
  		}, 
  		error => {
        this._snackBar.open("Veuillez vérifier votre nom d'utilisateur/mot de passe","Fermer", {
          duration: 5000,
        });
  			console.log(error);
  		}

  		)
  }
  }

}
