import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { EquipeListeDataSource, EquipeListeItem } from './equipe-liste-datasource';
import { EquipeService } from '../services/Equipe/equipe.service';
import { MatDialog } from '@angular/material/dialog';
import { EquipeDialogComponent } from './equipe-dialog/equipe-dialog.component';
import { Equipe } from '../model/Equipe';
import { AppSettings } from '../settings/app.settings';
import { AddEquipeComponent } from '../add-equipe/add-equipe.component';

@Component({
  selector: 'app-equipe-liste',
  templateUrl: './equipe-liste.component.html',
  styleUrls: ['./equipe-liste.component.css']
})
export class EquipeListeComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatTable, {static: true}) table: MatTable<EquipeListeItem>;
  dataSource: EquipeListeDataSource;
  displayedPhoto = AppSettings.APP_URL + 'Images/High/';

  role=localStorage.getItem("role");

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['pays', 'drapeau', 'add'];
constructor(private equipeService: EquipeService, public dialog: MatDialog) {}
  ngOnInit() {

    this.equipeService.findAll().subscribe(equipes => {
      this.dataSource = new EquipeListeDataSource(equipes);
 },
 err => {
   console.log(err);
 },
 () => {
      console.log(this.dataSource);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = this.dataSource;
 }
    );
  }
 openDialog(row : Equipe): void {
   
    const dialogRef = this.dialog.open(EquipeDialogComponent, {
      width: '500px',
      data: row.idEquipe
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
    });
  }
  openAddDialog(): void {
    const dialogRef = this.dialog.open(AddEquipeComponent, {
      width: '500px',
      data: null
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
    });
  }

}
