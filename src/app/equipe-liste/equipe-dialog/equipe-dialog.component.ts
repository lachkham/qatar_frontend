import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { JoueurListeComponent } from 'src/app/joueur-liste/joueur-liste.component';
import { MatTabChangeEvent } from '@angular/material';
import { StaffListComponent } from 'src/app/staff-list/staff-list.component';

@Component({
  selector: 'app-equipe-dialog',
  templateUrl: './equipe-dialog.component.html',
  styleUrls: ['./equipe-dialog.component.css']
})
export class EquipeDialogComponent implements OnInit {
  @ViewChild(JoueurListeComponent) private joueurliste: JoueurListeComponent;
  @ViewChild(StaffListComponent) private staffliste: StaffListComponent;


  onTabChanged(event: MatTabChangeEvent)  {
    
    this.joueurliste.initTabLoaded();
    this.staffliste.initTabLoaded();
    switch(event.index) { 
      case 0: { 
        console.log("tab changed: Joueur Liste");
        this.joueurliste.ngOnInit();
        break; 
      } 
      case 1: { 
        console.log("tab changed: Staff Liste");
        this.staffliste.ngOnInit();         
        break; 
      } 
      default: { 
         //statements; 
         break; 
      } 

    }

 }

  constructor(public dialogRef: MatDialogRef<EquipeDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: string) { }

  ngOnInit(): void {
    console.log(this.data);
  }

}
