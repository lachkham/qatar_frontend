import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { EquipeService } from '../services/Equipe/equipe.service';
import { JoueurService } from '../services/personnes/joueur/joueur.service';
import { Joueur } from '../model/Personnes/Joueur';
import { FileInput } from 'ngx-material-file-input';
import { ImageService } from '../services/image/image.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppSettings } from '../settings/app.settings';
interface DialogData {
  data: Joueur;
}
@Component({
  selector: 'app-add-joueur',
  templateUrl: './add-joueur.component.html',
  styleUrls: ['./add-joueur.component.css']
})
export class AddJoueurComponent implements OnInit {
  joueurForm = this.fb.group({
    firstName: [null, Validators.required],
    imageFile: [null],
    lastName: [null, Validators.required],
    datenaiss: [null, Validators.required],
    equipe: [null, Validators.required],
    cin: [null, Validators.compose([
      Validators.required, Validators.minLength(7), Validators.maxLength(8)])
    ],
    poste: ['free', Validators.required]
  });
  loaded = false;
  widthValue = 0;
  hasUnitNumber = false;
  photo: FileInput;
  displayedPhoto: any = '../assets/inconnu.jpg';
  equipes = [];
  joueur = new Joueur();

  constructor(private fb: FormBuilder,
              private equipeService: EquipeService,
              private joueurService: JoueurService,
              private imageService: ImageService,
              public dialogRef: MatDialogRef<AddJoueurComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {}


  ngOnInit() {
    console.log(this.data.data)
      if (this.data.data != null) {
        console.log(this.data.data)

      this.setForm();
      }

      this.equipeService.findAll().subscribe(data => {
        this.equipes = data;
      });
  }

  setForm() {
    this.joueurForm.get('cin').setValue(this.data.data.cin);
    this.joueurForm.get('cin').disable();
    this.joueurForm.get('poste').setValue(this.data.data.poste);
    this.joueurForm.get('equipe').setValue(this.data.data.equipe.idEquipe);
    this.joueurForm.get('datenaiss').setValue(new Date(this.data.data.dateNaiss));
    this.joueurForm.get('lastName').setValue(this.data.data.prenom);
    this.joueurForm.get('firstName').setValue(this.data.data.nom);
    this.displayedPhoto = AppSettings.APP_URL + 'Images/Low/' + this.data.data.image.id ;
  }

  onSubmit() {
    const dn = new Date(this.joueurForm.get('datenaiss').value);

    this.joueur.nom = this.joueurForm.get('firstName').value;
    this.joueur.prenom = this.joueurForm.get('lastName').value;
    this.joueur.poste = this.joueurForm.get('poste').value;
    this.joueur.cin = this.joueurForm.get('cin').value;
    this.joueur.equipe = this.equipes.filter(x => x.idEquipe === this.joueurForm.get('equipe').value)[0];
    this.joueur.dateNaiss = dn.getTime() ;
    this.joueur.blessure = false;
    this.photo = this.joueurForm.get('imageFile').value;
    console.log(this.joueur);
   
      if (this.joueurForm.valid) {
        if (this.data.data == null || this.data==undefined) {

        this.imageService.AddOne(this.photo.files[0]).subscribe(data => {
          this.joueur.image = data ;
        },
        err => {
          console.log(err);
        },
        () => {
          this.joueurService.AddOne(this.joueur).subscribe(data => {
            console.log(data);
          });
        });
        this.dialogRef.close();

      
      
    } else {

        if (this.photo != null) {
          this.imageService.AddOne(this.photo.files[0]).subscribe(data => {
            this.joueur.image = data ;
            console.log(data);
          },
          err => {
            console.log(err);
          },
          () => {
            this.joueurService.UpdateOne(this.joueur).subscribe(data => {
              console.log(data);
            });
          });
          this.dialogRef.close();
        } else {
          this.joueur.image = this.data.data.image;
          this.joueurService.UpdateOne(this.joueur).subscribe(data => {
            console.log(data);
          });
          this.dialogRef.close();

        }
    }
  }

  }

  onChange() {
    this.photo = this.joueurForm.get('imageFile').value;
    console.log(this.joueurForm.get('imageFile').value);
    const  myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.displayedPhoto =  myReader.result;
    };
    myReader.readAsDataURL(this.photo.files[0]);

  }
  load() {
    this.loaded = true;
    this.widthValue = 150;
  }
}
