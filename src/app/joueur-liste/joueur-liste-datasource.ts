import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';
import { Joueur } from '../model/Personnes/Joueur';

// TODO: Replace this with your own data model type
export interface JoueurListeItem {
  joueur: Joueur;
}

// TODO: replace this with real data from your application

/**
 * Data source for the JoueurListe view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class JoueurListeDataSource extends DataSource<JoueurListeItem> {
  data: JoueurListeItem[] = [];
  paginator: MatPaginator;
  sort: MatSort;
  j: JoueurListeItem;
  constructor(joueurs) {
    super();
    this.data = joueurs;
    // console.log(this.data);
  }


  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
   delete() {
    this.data = [];
    console.log('deleted');
    console.log(this.data);
    this.connect();
  }
  connect(): Observable<JoueurListeItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: JoueurListeItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: JoueurListeItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'Nom': return compare(a.joueur.nom, b.joueur.nom, isAsc);
        case 'Prenom': return compare(+a.joueur.prenom, +b.joueur.prenom, isAsc);
        case 'Poste': return compare(+a.joueur.poste, +b.joueur.poste, isAsc);
        case 'Equipe': return compare(+a.joueur.equipe, +b.joueur.equipe, isAsc);
        default: return 0;
      }
    });
  }
}



/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
