import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { JoueurListeDataSource, JoueurListeItem } from './joueur-liste-datasource';
import { JoueurService } from '../services/personnes/joueur/joueur.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Joueur } from '../model/Personnes/Joueur';
import { AppSettings } from '../settings/app.settings';
import { AddJoueurComponent } from '../add-joueur/add-joueur.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-joueur-liste',
  templateUrl: './joueur-liste.component.html',
  styleUrls: ['./joueur-liste.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class JoueurListeComponent implements  OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatTable, {static: true}) table: MatTable<JoueurListeItem>;
 
  @Input() idEquipe:Number;
  role=localStorage.getItem("role");

  dataSource: JoueurListeDataSource;
  expandedElement: Joueur | null;
  age;
  loaded = false;
  widthValue = 0;
  photo: any;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['Nom', 'Prenom', 'poste', 'equipe','add'];
constructor(private joueurService: JoueurService,
            public dialog: MatDialog,
           ) {}



            tabLoaded="none";      
            initTabLoaded(){
              this.tabLoaded="none"
            }   

ngOnInit() {
    this.tabLoaded="none";         
  console.log(this.idEquipe);
  console.log(this.idEquipe===null);

  if(this.idEquipe===undefined || this.idEquipe===null){
    this.joueurService.findAll().subscribe(joueurs => {
     this.dataSource = new JoueurListeDataSource(joueurs);

     console.log(this.tabLoaded);
    },
    err => {
      console.log(err);
    },
    () => {
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = this.dataSource;
      this.tabLoaded="block";         

    });
  }else {
    this.joueurService.findAllByIdEquipe(this.idEquipe).subscribe(joueurs => {
      this.dataSource = new JoueurListeDataSource(joueurs);
 
      console.log(this.tabLoaded);
     },
     err => {
       console.log(err);
     },
     () => {
       this.dataSource.sort = this.sort;
       this.dataSource.paginator = this.paginator;
       this.table.dataSource = this.dataSource;
       this.tabLoaded="block";         
 
     });
  }
  }
  


  click(row: Joueur) {


      if (this.expandedElement === row) {
        this.expandedElement = null;
    } else {
        this.loaded = false;
        this.widthValue = 0;
        this.photo = AppSettings.APP_URL + 'Images/Low/' + row.image.id ;
        this.expandedElement = row;
        const timeDiff = Math.abs(Date.now() - row.dateNaiss);
        this.age = Math.floor((timeDiff / (1000 * 3600 * 24)) / 365.25);

    }

//    this.expandedElement = this.expandedElement === row ? null : row;
  }

  load() {
    this.loaded = true;
    this.widthValue = 65;
  }

  openDialog(row): void {
    const dialogRef = this.dialog.open(AddJoueurComponent, {
      width: '500px',
      data: {data: row}
    });

  }
}
