import { Component, OnInit, Inject } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Spectateur } from 'src/app/model/Spectateur';
import { PartieService } from 'src/app/services/partie/partie.service';
import { PersonneService } from 'src/app/services/personnes/personne/personne.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SpectateurService } from 'src/app/services/spectateur/spectateur.service';
import { Partie } from '../model/Partie';
import { Personne } from '../model/Personnes/Personne';
import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas';  


interface DialogData {
  data: Spectateur;
  partie:Partie
}

@Component({
  selector: 'app-add-spectateur',
  templateUrl: './add-spectateur.component.html',
  styleUrls: ['./add-spectateur.component.css']
})
export class AddSpectateurComponent implements OnInit {
  isLinear=true;
spectateurForm = this.fb.group({
    nom: [null, Validators.required],
   
    prenom: [null, Validators.required],
    exp: [null, Validators.required],

        security: [null, Validators.required],
        ccnumber: [null, Validators.required],

  });
  loaded = false;
  widthValue = 0;
  hasUnitNumber = false;
  datepartie;
  parties = [];
  personnes=[];
  personne : Personne;
  datenaiss;
  spectateur = new Spectateur();
  qr=false;
  resspectateur:string="";

  constructor(private fb: FormBuilder,
              private spectateurService: SpectateurService,
              private partieService: PartieService,
              private personneService: PersonneService,
              public dialogRef: MatDialogRef<AddSpectateurComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {}


  ngOnInit() {
    console.log(this.data.partie)
    if(this.data.partie!=null){
      this.datepartie= new Date (this.data.partie.date);
      this.datepartie= this.datepartie.toLocaleDateString()+', '+this.datepartie.toLocaleTimeString()
      this.datenaiss=(new Date(localStorage.getItem("datenaiss"))).toLocaleDateString();
      this.personne = new Personne()
      this.personne.nom=localStorage.getItem("nom");
      this.personne.prenom=localStorage.getItem("prenom");

    }
    
      this.spectateur.codeTicket=0;
      this.spectateur.partie=this.data.partie
      
      this.personneService.getCurrentUser().subscribe(data=>{
      this.spectateur.spectateur=data;

      });
  }

  setForm() {
    this.spectateurForm.get('codeTicket').setValue(this.data.data.codeTicket);
    this.spectateurForm.get('partie').setValue(this.data.data.partie.idPartie);
    this.spectateurForm.get('spectateur').setValue(this.data.data.spectateur.nom);
   
   
  }
codeTicket;
  onSubmit() {

    this.spectateurService.AddOne(this.spectateur).subscribe(res=>{
      this.resspectateur=" Code Ticker: "+ res.codeTicket+
      "\n Nom et prenom Spectateur: "+res.spectateur.nom+" "+res.spectateur.prenom+
      "\n Partie: "+ res.partie.eq1.pays+" VS "+res.partie.eq2.pays
      this.codeTicket=res.codeTicket;
      console.log(this.resspectateur);
      this.qr=true;
    });
 
   

}

downloadPdf() {
  var data = document.getElementById('content');  
  html2canvas(data).then(canvas => {  
     
    const contentDataURL = canvas.toDataURL('image/png')  
    let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
    var position = 0;  
    pdf.addImage(contentDataURL, 'PNG', 50, 10)  
    pdf.save('Ticket N° '+this.codeTicket+'.pdf'); // Generated PDF   
  });  

}


}