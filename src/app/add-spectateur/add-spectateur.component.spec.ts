import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSpectateurComponent } from './add-spectateur.component';

describe('AddSpectateurComponent', () => {
  let component: AddSpectateurComponent;
  let fixture: ComponentFixture<AddSpectateurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSpectateurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSpectateurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
