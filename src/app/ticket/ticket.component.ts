import { Component, OnInit, Inject } from '@angular/core';
import { Spectateur } from '../model/Spectateur';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas';  


interface DialogData {
  data: Spectateur;
}
@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})
export class TicketComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<TicketComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }
    datenaiss;
    datepartie;
    resspectateur;
  ngOnInit(): void {
    this.datepartie= new Date (this.data.data.partie.date);
    this.datepartie= this.datepartie.toLocaleDateString()+', '+this.datepartie.toLocaleTimeString()
    this.datenaiss=(new Date(this.data.data.spectateur.dateNaiss)).toLocaleDateString();
    this.resspectateur=" Code Ticker: "+ this.data.data.codeTicket+
    "\n Nom et prenom Spectateur: "+this.data.data.spectateur.nom+" "+this.data.data.spectateur.prenom+
    "\n Partie: "+ this.data.data.partie.eq1.pays+" VS "+this.data.data.partie.eq2.pays
  }

  downloadPdf() {
    var data = document.getElementById('content');  
    html2canvas(data).then(canvas => {  
       
      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;  
      pdf.addImage(contentDataURL, 'PNG', 25, 10)  
      pdf.save('Ticket N° '+this.data.data.codeTicket+'.pdf'); // Generated PDF   
    });  

}
 
}
