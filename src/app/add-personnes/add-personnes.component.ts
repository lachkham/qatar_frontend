import { Component, OnInit, Inject } from '@angular/core';
import { FileInput } from 'ngx-material-file-input';
import { Validators, FormBuilder } from '@angular/forms';
import { PartieService } from 'src/app/services/partie/partie.service';
import { ImageService } from 'src/app/services/image/image.service';
import { PersonneService } from 'src/app/services/personnes/personne/personne.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Personne } from 'src/app/model/Personnes/Personne';
import { AppSettings } from 'src/app/settings/app.settings';
interface DialogData {
  data: Personne;
}
@Component({
  selector: 'app-add-personnes',
  templateUrl: './add-personnes.component.html',
  styleUrls: ['./add-personnes.component.css']
})
export class AddPersonnesComponent implements OnInit {

  personneForm = this.fb.group({
    
    firstName: [null, Validators.required],
    lastName: [null, Validators.required],
    username: [null, Validators.required],
    password: [null, Validators.required],
    cin: [null, Validators.compose([
    Validators.required, Validators.minLength(7), Validators.maxLength(8)])],
    datenaiss: [null, Validators.required],
    imageFile: [null],
    
    
  });
  loaded = false;
  widthValue = 0;
  hasUnitNumber = false;
  photo: FileInput;
  displayedPhoto: any = '../assets/inconnu.jpg';
  
  parties=[];
personne=new Personne();


  constructor(private fb: FormBuilder,
    private imageService: ImageService,
    private personneService:PersonneService,
    public dialogRef: MatDialogRef<AddPersonnesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}


    ngOnInit() {
      if (this.data != null) {
      this.setForm();
      }

     
  }
  setForm() {
    this.personneForm.get('cin').setValue(this.data.data.cin);
    this.personneForm.get('cin').disable();
    this.personneForm.get('lastName').setValue(this.data.data.nom);
    
    
    this.personneForm.get('firstName').setValue(this.data.data.prenom);
    this.personneForm.get('datenaiss').setValue(new Date(this.data.data.dateNaiss));
    this.displayedPhoto = AppSettings.APP_URL + 'Images/Low/' + this.data.data.image.id ;


  }







  onSubmit() {
    const dn = new Date(this.personneForm.get('datenaiss').value);

    this.personne.nom = this.personneForm.get('firstName').value;
    this.personne.prenom = this.personneForm.get('lastName').value;
    this.personne.username=this.personneForm.get('username').value;
    this.personne.password=this.personneForm.get('password').value;
    this.personne.role=[];
    this.personne.cin = this.personneForm.get('cin').value;
    this.personne.dateNaiss = dn.getTime() ;
    this.photo = this.personneForm.get('imageFile').value;
    console.log(this.personne);
    console.log(this.personneForm.valid);

    if (this.personneForm.valid) {
      if (this.data==null||this.data.data == null || this.data==undefined) {

      this.imageService.AddOne(this.photo.files[0]).subscribe(data => {
        this.personne.image = data ;
      },
      err => {
        console.log(err);
      },
      () => {
        this.personneService.AddOne(this.personne).subscribe(data => {
          console.log(data);
        });
      });
      this.dialogRef.close();

    
  } else {

      if (this.photo != null) {
        this.imageService.AddOne(this.photo.files[0]).subscribe(data => {
          this.personne.image = data ;
          console.log(data);
        },
        err => {
          console.log(err);
        },
        () => {
          this.personneService.UpdateOne(this.personne).subscribe(data => {
            console.log(data);
          });
        });
        this.dialogRef.close();
      } else {
        this.personne.image = this.data.data.image;
        this.personneService.UpdateOne(this.personne).subscribe(data => {
          console.log(data);
        });
        this.dialogRef.close();

      }
  }
}

}

  onChange() {
    this.photo = this.personneForm.get('imageFile').value;
    console.log(this.personneForm.get('imageFile').value);
    const  myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.displayedPhoto =  myReader.result;
    };
    myReader.readAsDataURL(this.photo.files[0]);

  }
  load() {
    this.loaded = true;
    this.widthValue = 150;
  }



}
