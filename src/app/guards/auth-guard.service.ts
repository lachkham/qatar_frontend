import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginService } from '../services/personnes/login.service';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor (private auth: LoginService , 
                private router : Router){}
  loggedIn

  canActivate(next: ActivatedRouteSnapshot,state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    
    
    if(localStorage.getItem('xAuthToken')!=null){
      return true
    }
    this.router.navigate(['/']);
    return false;

  }
}

