import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginService } from '../services/personnes/login.service';
import { Observable } from 'rxjs';

@Injectable()

export class LoginGuardService implements CanActivate {
  constructor (private auth: LoginService , private router : Router){}
  loggedIn

  canActivate(next: ActivatedRouteSnapshot,state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      
    if(localStorage.getItem('xAuthToken')!=null){
      this.router.navigate(['/dashboard']);
      return false
    }
    return true;

  }
}

